from tornado.options import define, options

define('port', default=8000, help='run on given port', type=int)
define('db.uri', default='localhost', help='mongodb uri')
define('db.name', default='photo_hosting', help='name of database')
define('debug', default=True, help='debug mode', type=bool)


